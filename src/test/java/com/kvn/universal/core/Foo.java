package com.kvn.universal.core;
/**
* @author wzy
* @date 2017年8月2日 下午6:12:10
*/
public class Foo {
	private long id;
	private String name;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
