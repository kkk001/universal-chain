package com.kvn.universal.core;

import java.io.Reader;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import com.kvn.universal.dao.IIdempotentDao;
import com.kvn.universal.filter.lock.RedisProvider;

/**
 * @author wzy
 * @date 2017年8月2日 下午5:06:24
 */
public class InvokerBuilderTest {
	private static SqlSessionFactory sqlSessionFactory = null;

	@Before
	public void setUp() throws Exception {
		Reader reader = Resources.getResourceAsReader("mybatis-config.xml");
		// 建立SqlSessionFactory
		sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader);
	}

	@Test
	public void test() {
		initPool();
		// 打开Session
		SqlSession session = sqlSessionFactory.openSession();
		IIdempotentDao idempotentDao = session.getMapper(IIdempotentDao.class);
		
		BizService bizService = new BizService(idempotentDao);
		bizService.doBiz("hehexx"); // 测试链
//		bizService.doBizUseCallbackIdempotent("hehehe"); // 测试链中 callback
		
		session.commit();
		session.close();
		destroyPool();
	}
	
	private static void destroyPool() {
		RedisProvider.destroy();
	}

	private static void initPool() {
		RedisProvider redisProvider = new RedisProvider();
		redisProvider.setRedisHost("172.16.21.14");
		redisProvider.setRedisPort(6379);
		redisProvider.setMaxIdle(5);
		redisProvider.setMaxTotal(3000);
		redisProvider.setMaxWaitMillis(10000);
		redisProvider.setTestOnBorrow(true);
		RedisProvider.init();
	}

}
