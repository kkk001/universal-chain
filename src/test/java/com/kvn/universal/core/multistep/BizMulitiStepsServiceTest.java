package com.kvn.universal.core.multistep;

import java.io.Reader;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.kvn.universal.dao.IIdempotentDao;
import com.kvn.universal.filter.lock.RedisProvider;

/**
* @author wzy
* @date 2017年10月11日 下午3:02:36
*/
public class BizMulitiStepsServiceTest {
	private static SqlSessionFactory sqlSessionFactory = null;
	private SqlSession sqlSession = null;
	private IIdempotentDao idempotentDao = null;

	@BeforeClass
	public static void init() throws Exception {
		initRedis();
		
		Reader reader = Resources.getResourceAsReader("mybatis-config.xml");
		// 建立SqlSessionFactory & sqlSession
		sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader);
	}
	
	@AfterClass
	public static void destroy() {		
		RedisProvider.destroy();
	}
	
	@Before
	public void before() {
		sqlSession = sqlSessionFactory.openSession();
		idempotentDao = sqlSession.getMapper(IIdempotentDao.class);
	}
	
	@After
	public void after() {
		sqlSession.commit();
		sqlSession.close();
	}
	
	private static void initRedis() {
		RedisProvider redisProvider = new RedisProvider();
		redisProvider.setRedisHost("172.16.21.14");
		redisProvider.setRedisPort(6379);
		redisProvider.setMaxIdle(5);
		redisProvider.setMaxTotal(3000);
		redisProvider.setMaxWaitMillis(10000);
		redisProvider.setTestOnBorrow(true);
		redisProvider.init();
	}

	/**
	 * 老接口
	 */
	@Test
	@Deprecated
	public void testDoBiz() {
		BizMulitiStepsService bizService = new BizMulitiStepsService(idempotentDao);
		bizService.doBiz("hehexx"); // 测试链
	}
	
	/**
	 * 幂等新接口
	 */
	@Test
	public void testDoBizNew() {
		BizMulitiStepsService bizService = new BizMulitiStepsService(idempotentDao);
		bizService.doBizNew("hehexxNEW"); // 测试链
	}
	
	/**
	 * 幂等&锁新接口
	 */
	@Test
	public void testDoBizNew2(){
		BizMulitiStepsService bizService = new BizMulitiStepsService(idempotentDao);
		bizService.doBizNew2("hehexxNEW"); // 测试链
	}

}
