package com.kvn.universal.dao;

import org.apache.ibatis.annotations.Param;

import com.kvn.universal.domain.Idempotent;

public interface IIdempotentDao {

    int add(Idempotent record);

	int updateStatus(@Param("idempotentId")String idempotentId,
					 @Param("businessId") String businessId,
					 @Param("stepId") String stepId,
					 @Param("newStatus")String newStatus,
					 @Param("oldStatus")String oldStatus,
					 @Param("returnInfo")String returnInfo,
					 @Param("returnByte") byte[] returnByte,
					 @Param("returnClass")String returnClass,
					 @Param("remark")String remark);

	Idempotent selectOne(@Param("idempotentId")String idempotentId,
						 @Param("businessId") String businessId,
						 @Param("stepId") String stepId);

}