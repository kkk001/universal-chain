package com.kvn.universal.exception;
/**
* @author wzy
* @date 2017年8月18日 下午2:52:14
*/
public class UniversalException extends RuntimeException {
	private static final long serialVersionUID = 1L;

    /**
     * 异常错误码
     */
    protected Integer code;

    /**
     * 异常信息的参数
     */
    protected Object[] args;

    public UniversalException() {
        super();
    }

    public UniversalException(String message) {
        super(message);
    }

    public UniversalException(String message, Throwable cause) {
        super(message, cause);
    }

    public UniversalException(String message, Object[] args) {
        this(null, message, null, args);
    }
    
    public UniversalException(String message, Throwable cause, Object[] args) {
        this(null, message, cause, args);
    }
    
    public UniversalException(Integer code, String message) {
        this(code, message, null, null);
    }

    public UniversalException(Integer code, String message, Throwable cause) {
        this(code, message, cause, null);
    }

    public UniversalException(Integer code, String message, Object[] args) {
        this(code, message, null, args);
    }

    /**
     * 
     * @param code
     *            错误码
     * @param message
     *            错误信息
     * @param cause
     *            原始异常
     * @param args
     *            额外参数
     */
    public UniversalException(Integer code, String message, Throwable cause, Object[] args) {
        super(message, cause);
        this.code = code;
        this.args = args;
    }

    public Integer getCode() {
        return code;
    }

    public Object[] getArgs() {
        return args;
    }
}
