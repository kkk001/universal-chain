package com.kvn.universal.filter.idempotent;

/**
 * 幂等结果序列化类型
 * 
 * @author wzy
 * @date 2017年8月18日 下午3:20:58
 */
public enum SerializationType {
	HESSIAN, JSON;
}
