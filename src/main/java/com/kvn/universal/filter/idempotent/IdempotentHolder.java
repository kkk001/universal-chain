package com.kvn.universal.filter.idempotent;

import com.google.common.base.Strings;

/**
 * 持有幂等信息
 * 
 * @author wzy
 * @date 2017年8月2日 上午11:25:13
 */
public class IdempotentHolder {
	private String idempotentId;
	private String businessId;
	private String stepId;
	private String unionKey; // 联合key = idempotentId + businessId + stepId

	public String getIdempotentId() {
		return idempotentId;
	}
	public void setIdempotentId(String idempotentId) {
		this.idempotentId = idempotentId;
	}
	public String getBusinessId() {
		return businessId;
	}
	public void setBusinessId(String businessId) {
		this.businessId = businessId;
	}
	public String getStepId() {
		return stepId;
	}
	public void setStepId(String stepId) {
		this.stepId = stepId;
	}
	public String getUnionKey() {
		return unionKey;
	}
	public void setUnionKey(String unionKey) {
		this.unionKey = unionKey;
	}
	
	public static IdempotentHolder create(String idempotentId) {
		return create(idempotentId, null);
	}

	public static IdempotentHolder create(String idempotentId, String businessId){
		return create(idempotentId, businessId, null);
	}
	
	public static IdempotentHolder create(String idempotentId, String businessId, String stepId){
		IdempotentHolder holder = new IdempotentHolder();
		holder.setIdempotentId(idempotentId);
		holder.setBusinessId(businessId);
		holder.setStepId(stepId);
		String unionKey = idempotentId + Strings.nullToEmpty(businessId) + Strings.nullToEmpty(stepId);
		holder.setUnionKey(unionKey);
		return holder;
	}
	
	@Override
	public String toString() {
		return "IdempotentHolder [idempotentId=" + idempotentId + ", businessId=" + businessId + ", stepId=" + stepId + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((businessId == null) ? 0 : businessId.hashCode());
		result = prime * result + ((idempotentId == null) ? 0 : idempotentId.hashCode());
		result = prime * result + ((stepId == null) ? 0 : stepId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		
		IdempotentHolder other = (IdempotentHolder) obj;
		if(!this.unionKey.equals(other.getUnionKey())){
			return false;
		}
		return true;
	}
	
}
