package com.kvn.universal.filter;

import com.kvn.universal.core.InvokerContext;

/**
 * 回调接口。
 * 
 * @author wzy
 * @date 2017年8月2日 下午4:59:14
 */
public interface Callback {
	Object call(InvokerContext context);
}
