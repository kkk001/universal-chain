package com.kvn.universal.filter;

import com.kvn.universal.core.Invoker;
import com.kvn.universal.core.InvokerContext;

public interface Filter {
	
	Object invokeWapper(Invoker invoker, InvokerContext lockContext);

}
