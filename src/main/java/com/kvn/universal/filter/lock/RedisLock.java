package com.kvn.universal.filter.lock;

import java.util.concurrent.locks.Lock;

/**
* @author wzy
* @date 2017年8月1日 下午4:46:44
*/
public interface RedisLock extends Lock {
    /**
     * 获取当前锁对象状态
     * @return 上锁是否成功
     */
    boolean isLocked();
}
