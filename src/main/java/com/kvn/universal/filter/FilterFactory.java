package com.kvn.universal.filter;

import com.kvn.universal.dao.IIdempotentDao;
import com.kvn.universal.filter.idempotent.IdempotentFilter;
import com.kvn.universal.filter.lock.LockFilter;

/**
* @author wzy
* @date 2017年8月2日 下午12:52:04
*/
public abstract class FilterFactory {
	
	public static Filter createLockFilter(){
		return new LockFilter();
	}
	
	public static Filter createIdempodentFilter(IIdempotentDao idempotentDao){
		return new IdempotentFilter(idempotentDao);
	}
	
	public static Filter createIdempodentFilter(IIdempotentDao idempotentDao, Callback callback){
		return new IdempotentFilter(idempotentDao, callback);
	}
	
}
