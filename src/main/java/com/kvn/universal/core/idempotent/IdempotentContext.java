package com.kvn.universal.core.idempotent;

import java.util.List;

import com.google.common.collect.Lists;
import com.kvn.universal.core.ExtendContext;
import com.kvn.universal.domain.Idempotent;
import com.kvn.universal.filter.idempotent.IdempotentHolder;

/**
 * 幂等上下文
 * 
 * @author wzy
 * @date 2017年7月14日 下午1:47:49
 */
public class IdempotentContext implements ExtendContext {
	private IdempotentHolder currentIdempotent;
	// 分步幂等中，每一个幂等方法的返回结果
	private List<Object> stepRltList = Lists.newArrayList();
	// 分步幂等中，存放在数据库中的幂等记录
	private List<Idempotent> stepIdempotentList = Lists.newArrayList();
	
	public IdempotentContext(IdempotentHolder currentIdempotent) {
		super();
		this.currentIdempotent = currentIdempotent;
	}
	

	public IdempotentContext() {
		super();
	}

	public static IdempotentContext create(IdempotentHolder currentIdempotent){
		return new IdempotentContext(currentIdempotent);
	}
	
	public static IdempotentContext create(){
		return new IdempotentContext();
	}
	
	public IdempotentContext addStepRlt(Object rlt){
		stepRltList.add(rlt);
		return this;
	}
	
	public IdempotentContext addStepIdempotent(Idempotent idempotent){
		stepIdempotentList.add(idempotent);
		return this;
	}

	public IdempotentHolder getCurrentIdempotent() {
		return currentIdempotent;
	}

	public void setCurrentIdempotent(IdempotentHolder currentIdempotent) {
		this.currentIdempotent = currentIdempotent;
	}

	public List<Object> getStepRltList() {
		return stepRltList;
	}
}
