package com.kvn.universal.core.idempotent;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.google.common.collect.Lists;
import com.kvn.universal.core.InvokerBuilder;
import com.kvn.universal.core.InvokerContext;
import com.kvn.universal.dao.IIdempotentDao;
import com.kvn.universal.filter.FilterFactory;

/**
 * 幂等步骤容器
 * 
 * @author wzy
 * @date 2017年10月11日 下午3:37:25
 */
public class StepsContainer {
	private IIdempotentDao idempotentDao;
	private List<IdempotentStep> stepList;

	public static StepsContainer create(IIdempotentDao idempotentDao) {
		StepsContainer container = new StepsContainer();
		return container.initIdempotentDao(idempotentDao);
	}

	public StepsContainer initIdempotentDao(IIdempotentDao idempotentDao) {
		this.idempotentDao = idempotentDao;
		return this;
	}

	public StepsContainer addStep(IdempotentStep step) {
		if (stepList == null) {
			stepList = Lists.newArrayList();
		}
		stepList.add(step);
		return this;
	}

	public void executeAll(InvokerContext context) {
		if (CollectionUtils.isEmpty(stepList)) {
			return;
		}
		context.putExtendContext(IdempotentContext.create());

		// 链上所有的操作共用同一个上下文
		for (IdempotentStep step : stepList) {
			context.getExtendContext(IdempotentContext.class).setCurrentIdempotent(step.getIdempotentHolder());
			Object rlt = InvokerBuilder.buildInvoker(step, context, FilterFactory.createIdempodentFilter(idempotentDao)).delegateInvokeInvoker();
			context.getExtendContext(IdempotentContext.class).addStepRlt(rlt);
		}
	}

}
