package com.kvn.universal.core.idempotent;

import com.kvn.universal.core.Invoker;
import com.kvn.universal.core.InvokerBuilder;
import com.kvn.universal.core.InvokerContext;
import com.kvn.universal.dao.IIdempotentDao;
import com.kvn.universal.filter.FilterFactory;

/**
 * 在锁中执行每一个幂等步骤容器
 * 
 * @author wzy
 * @date 2017年10月12日 下午2:51:18
 */
public class StepsWithinLockContainer extends StepsContainer {

	public static StepsContainer create(IIdempotentDao idempotentDao) {
		StepsWithinLockContainer container = new StepsWithinLockContainer();
		return container.initIdempotentDao(idempotentDao);
	}

	@Override
	public void executeAll(InvokerContext context) {
		Invoker lockInvoker = new Invoker() {
			@Override
			public Object invoke(InvokerContext invokerContext) {
				return executeAllIdempotentSteps(invokerContext);
			}
		};

		InvokerBuilder.buildInvoker(lockInvoker, context, FilterFactory.createLockFilter()).delegateInvokeInvoker();
	}

	protected Object executeAllIdempotentSteps(InvokerContext context) {
		super.executeAll(context);
		return null;
	}

}
