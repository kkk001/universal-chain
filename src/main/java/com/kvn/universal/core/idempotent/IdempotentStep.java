package com.kvn.universal.core.idempotent;

import com.kvn.universal.core.Invoker;
import com.kvn.universal.filter.idempotent.IdempotentHolder;

/**
 * 幂等的操作
 * 
 * @author wzy
 * @date 2017年10月11日 下午2:43:35
 */
public abstract class IdempotentStep implements Invoker{
	// 当前步骤的幂等对象。由调用者控制，防止步骤顺序变换后产生错误
	private IdempotentHolder idempotentHolder;
	
	public IdempotentStep(IdempotentHolder idempotentHolder) {
		super();
		this.idempotentHolder = idempotentHolder;
	}

	public IdempotentHolder getIdempotentHolder() {
		return idempotentHolder;
	}

}
