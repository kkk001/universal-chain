package com.kvn.universal.core;

/**
* @author wzy
* @date 2017年7月14日 下午12:23:54
*/
public interface Invoker {
	
	Object invoke(InvokerContext lockContext);

}
