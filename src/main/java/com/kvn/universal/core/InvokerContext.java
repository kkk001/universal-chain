package com.kvn.universal.core;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;

/**
 * lock的上下文
 * 
 * @author wzy
 * @date 2017年7月14日 下午1:47:49
 */
public class InvokerContext {
	// 业务参数
	private List<Object> args;
	// 链中公共的上下文参数
	private Map<String, Object> attachment = new HashMap<>();
	// 扩展上下文
	private  Map<Class<? extends ExtendContext>, ExtendContext> extendContextMap = new HashMap<>();

	public void putExtendContext(ExtendContext extendContext){
		extendContextMap.put(extendContext.getClass(), extendContext);
	}
	
	public <T extends ExtendContext> T getExtendContext(Class<T> contextClass){
		return (T) extendContextMap.get(contextClass);
	}
	
	public static InvokerContext create() {
		return new InvokerContext();
	}

	public InvokerContext apendArg(Object arg) {
		if (args == null) {
			args = Lists.newArrayList();
		}
		args.add(arg);
		return this;
	}

	public List<Object> getArgs() {
		return args;
	}

	public void setArgs(List<Object> args) {
		this.args = args;
	}

	public Map<String, Object> getAttachment() {
		return attachment;
	}

	public void setAttachment(Map<String, Object> attachment) {
		this.attachment = attachment;
	}
	
}

