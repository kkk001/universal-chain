package com.kvn.universal.core.lock;

import com.kvn.universal.filter.lock.RedisReentrantLock;

/**
 * @author wzy
 * @date 2017年7月25日 上午10:17:25
 */
public class LockHolder {
	private String key;
	private String value;
	private RedisReentrantLock lock;

	public static LockHolder create(String key, String value){
		return new LockHolder(key, value);
	}
	
	public static LockHolder create(String key){
		return new LockHolder(key);
	}
	
	
	public LockHolder(String key, String value) {
		super();
		this.key = key;
		this.value = value;
	}


	public LockHolder(String key) {
		super();
		this.key = key;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public RedisReentrantLock getLock() {
		return lock;
	}
	
	public void setLock(RedisReentrantLock lock) {
		this.lock = lock;
	}

	@Override
	public String toString() {
		return "LockHolder [key=" + key + ", value=" + value + "]";
	}



}
