package com.kvn.universal.core.lock;

import java.util.ArrayList;

import com.google.common.collect.Lists;
import com.kvn.universal.core.ExtendContext;

/**
 * lock的上下文
 * 
 * @author wzy
 * @date 2017年7月14日 下午1:47:49
 */
public class LockContext implements ExtendContext {
	// 可以一次上多个锁
	private ArrayList<LockHolder> lockHolderLs = Lists.newArrayList();

	public static LockContext create() {
		return new LockContext();
	}

	public LockContext appendLock(LockHolder lock) {
		this.lockHolderLs.add(lock);
		return this;
	}

	public LockContext appendLockList(ArrayList<LockHolder> lockLs) {
		this.lockHolderLs.addAll(lockLs);
		return this;
	}

	public ArrayList<LockHolder> getLockHolderLs() {
		return lockHolderLs;
	}

	public void setLockHolderLs(ArrayList<LockHolder> lockHolderLs) {
		this.lockHolderLs = lockHolderLs;
	}

}
