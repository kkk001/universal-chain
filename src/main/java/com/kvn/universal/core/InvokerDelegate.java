package com.kvn.universal.core;

/**
 * @author wzy
 * @date 2017年7月14日 下午1:59:51
 */
public abstract class InvokerDelegate implements Invoker {
	private InvokerContext lockContext;

	public InvokerDelegate(InvokerContext lockContext) {
		super();
		this.lockContext = lockContext;
	}

	public Object delegateInvokeInvoker() {
		return invoke(lockContext);
	}

}
