# universal-chain

## 简介  
universal-chain是一个万用链，它能将将公共逻辑与业务逻辑解耦。而且公共逻辑可以方便的进行扩展，而不影响业务逻辑。  
使用的是责任链的思想。通过代理的方式，提供了简单易用的API。  
现在universal-chain提供了以下功能：  
* 1.  幂等链（IdempotentFilter.java）  
* 2.  Redis分布式可重入锁链（LockFilter.java）  

## Quick Start
示例参考：[InvokerBuilderTest.java](src/test/java/com/kvn/universal/core/InvokerBuilderTest.java)  
（幂等功能依赖表：[idempotent](src/main/resources/universal/table.sql)）  
>
	public class BizService implements Invoker {
		private IIdempotentDao idempotentDao;
		public String doBiz(String param) {
			InvokerContext context = InvokerContext.create().apendArg(param);
			context.putExtendContext(LockContext.create().appendLock(LockHolder.create("wzy", null)));
			String rlt = (String) InvokerBuilder
					.buildInvoker(this, context, FilterFactory.createLockFilter())
					.delegateInvokeInvoker();
			System.out.println("==>执行结果：" + rlt);
			return rlt;
		}
		@Override
		public Object invoke(InvokerContext lockContext) {
			String param = (String) lockContext.getArgs().get(0);
			System.out.println("---> 执行业务:param=" + param);
			return "OK";
		}
	}
	
### 分步幂等最佳实践：  
示例：[BizMulitiStepsServiceTest](src/test/java/com/kvn/universal/core/multistep/BizMulitiStepsServiceTest.java)
>
	public void doBizNew2(final String param) {
		IdempotentStep step1 = new IdempotentStep(IdempotentHolder.create("幂等key-new", "业务1", "step1")) {
			@Override
			public Object invoke(InvokerContext invokerContext) {
				String step1rlt = step1(param);
				return step1rlt;
			}
		};
		IdempotentStep step2 = new IdempotentStep(IdempotentHolder.create("幂等key-new", "业务1", "step2")) {
			@Override
			public Object invoke(InvokerContext invokerContext) {
				return step2(param, invokerContext);
			}
		};
		IdempotentStep step3 = new IdempotentStep(IdempotentHolder.create("幂等key-new", "业务1", "step3")) {
			@Override
			public Object invoke(InvokerContext invokerContext) {
				return step3(param);
			}
		};
		// 1. 不用锁的写法
		StepsContainer.create(idempotentDao).addStep(step1).addStep(step2).addStep(step3).executeAll(InvokerContext.create());
		// 2. 用锁的写法
		// InvokerContext context = InvokerContext.create();
		// context.putExtendContext(LockContext.create().appendLock(LockHolder.create("wzy")));
		// StepsWithinLockContainer.create(idempotentDao).addStep(step1).addStep(step2).addStep(step3).executeAll(context);
	}

## 幂等链（IdempotentFilter.java）  
注意：使用幂等链时，需要外部注入 IIdempotentDao。  
在使用时，需要在使用者的容器中加载 MyBatis的 [IIdempotentDao.java](src/main/java/com/kvn/universal/dao/IIdempotentDao.java) 和 [IdempotentMapper.xml](src/main/resources/universal/mapper/IdempotentMapper.xml)  


## Redis分布式可重入锁链（LockFilter.java）  
使用的是 Redis 做分布式锁。可重入性是使用记数器实现的。  